//=============================================================================================
// Szamitogepes grafika hazi feladat keret. Ervenyes 2014-tol.          
// A //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// sorokon beluli reszben celszeru garazdalkodni, mert a tobbit ugyis toroljuk. 
// A beadott program csak ebben a fajlban lehet, a fajl 1 byte-os ASCII karaktereket tartalmazhat. 
// Tilos:
// - mast "beincludolni", illetve mas konyvtarat hasznalni
// - faljmuveleteket vegezni (printf is fajlmuvelet!)
// - new operatort hivni az onInitialization függvényt kivéve, a lefoglalt adat korrekt felszabadítása nélkül 
// - felesleges programsorokat a beadott programban hagyni
// - tovabbi kommenteket a beadott programba irni a forrasmegjelolest kommentjeit kiveve
// ---------------------------------------------------------------------------------------------
// A feladatot ANSI C++ nyelvu forditoprogrammal ellenorizzuk, a Visual Studio-hoz kepesti elteresekrol
// es a leggyakoribb hibakrol (pl. ideiglenes objektumot nem lehet referencia tipusnak ertekul adni)
// a hazibeado portal ad egy osszefoglalot.
// ---------------------------------------------------------------------------------------------
// A feladatmegoldasokban csak olyan gl/glu/glut fuggvenyek hasznalhatok, amelyek
// 1. Az oran a feladatkiadasig elhangzottak ES (logikai AND muvelet)
// 2. Az alabbi listaban szerepelnek:  
// Rendering pass: glBegin, glVertex[2|3]f, glColor3f, glNormal3f, glTexCoord2f, glEnd, glDrawPixels
// Transzformaciok: glViewport, glMatrixMode, glLoadIdentity, glMultMatrixf, gluOrtho2D, 
// glTranslatef, glRotatef, glScalef, gluLookAt, gluPerspective, glPushMatrix, glPopMatrix,
// Illuminacio: glMaterialfv, glMaterialfv, glMaterialf, glLightfv
// Texturazas: glGenTextures, glBindTexture, glTexParameteri, glTexImage2D, glTexEnvi, 
// Pipeline vezerles: glShadeModel, glEnable/Disable a kovetkezokre:
// GL_LIGHTING, GL_NORMALIZE, GL_DEPTH_TEST, GL_CULL_FACE, GL_TEXTURE_2D, GL_BLEND, GL_LIGHT[0..7]
//
// NYILATKOZAT
// ---------------------------------------------------------------------------------------------
// Nev    : Barta Gergo
// Neptun : YM4TZ1
// ---------------------------------------------------------------------------------------------
// ezennel kijelentem, hogy a feladatot magam keszitettem, es ha barmilyen segitseget igenybe vettem vagy 
// mas szellemi termeket felhasznaltam, akkor a forrast es az atvett reszt kommentekben egyertelmuen jeloltem. 
// A forrasmegjeloles kotelme vonatkozik az eloadas foliakat es a targy oktatoi, illetve a 
// grafhazi doktor tanacsait kiveve barmilyen csatornan (szoban, irasban, Interneten, stb.) erkezo minden egyeb 
// informaciora (keplet, program, algoritmus, stb.). Kijelentem, hogy a forrasmegjelolessel atvett reszeket is ertem, 
// azok helyessegere matematikai bizonyitast tudok adni. Tisztaban vagyok azzal, hogy az atvett reszek nem szamitanak
// a sajat kontribucioba, igy a feladat elfogadasarol a tobbi resz mennyisege es minosege alapjan szuletik dontes.  
// Tudomasul veszem, hogy a forrasmegjeloles kotelmenek megsertese eseten a hazifeladatra adhato pontokat 
// negativ elojellel szamoljak el es ezzel parhuzamosan eljaras is indul velem szemben.
//=============================================================================================

#define _USE_MATH_DEFINES
#include <math.h>
#include <stdlib.h>

#if defined(__APPLE__)
#include <OpenGL/gl.h>                                                                                                                                                                                                            
#include <OpenGL/glu.h>                                                                                                                                                                                                           
#include <GLUT/glut.h>                                                                                                                                                                                                            
#else
#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__)
#include <windows.h>                                                                                                                                                                                                              
#endif
#include <GL/gl.h>                                                                                                                                                                                                                
#include <GL/glu.h>                                                                                                                                                                                                               
#include <GL/glut.h>                                                                                                                                                                                                              
#endif
#define PI (3.14159265f)


const int screenWidth = 1000;	// alkalmazás ablak felbontása
const int screenHeight = 1000;
bool animate = false;
long i_time;
long d_time;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Innentol modosithatod...

struct Color {
    float r, g, b;

    Color( ) {
        r = g = b = 0;
    }
    Color(float r0, float g0, float b0) {
        r = r0; g = g0; b = b0;
    }
    Color operator*(float a) {
        return Color(r * a, g * a, b * a);
    }
    Color operator*(const Color& c) {
        return Color(r * c.r, g * c.g, b * c.b);
    }
    Color operator+(const Color& c) {
        return Color(r + c.r, g + c.g, b + c.b);
    }
};

struct float3 {
    float x, y, z;

    float3(float x0=0.0f, float y0=0.0f, float z0=0.0f) { x=x0; y=y0; z=z0; }

    float3 operator*(float a) {
        return float3(x * a, y * a, z * a);
    }
    float3 operator+(const float3& v) {
        return float3(x + v.x, y + v.y, z + v.z);
    }
    float3 operator+(const float& v) {
        return float3(x + x, y + y, z + z);
    }
    float3 operator-(const float3& v) {
        return float3(x - v.x, y - v.y, z - v.z);
    }
    float3 operator/(const float& v) {
        return float3(x / v, y / v, z / v);
    }

    float Length() { return sqrtf(x * x + y * y + z * z); }
};

struct Scene;

float dot(const float3& v1, const float3& v2) {
    return (v1.x * v2.x + v1.y * v2.y + v1.z * v2.z);
}

float3 rotateZ(const float3& cp, const float& angle) {
    return float3((float) (cp.x * cos(angle) - cp.y * sin(angle)),
                  (float) (cp.x * sin(angle) + cp.y * cos(angle)),
                  0.0f);
}

float3 translate(float3& cp, float3& trvec) {
    return cp - trvec;
}

float3 trans_vec = float3(0.018f, 0.02f, 0.0f);
float3 orig;

void scaleWorld() {
    glMatrixMode(GL_MODELVIEW);
    glScalef(2, 2, 1);
    glTranslatef(-250.0f, -250.0f, 0);
    glutPostRedisplay();
}
/* Animacio az elozo hazibol meritve */
void animate_w(long p_time, long e_time){
    if(animate) {
        for(long i = p_time; i < e_time; i++) {
            glMatrixMode(GL_MODELVIEW);
            orig = orig + trans_vec;
            if(orig.x < -250 || orig.x > 250) {
                trans_vec.x = -1.0f * trans_vec.x;

            }
            if(orig.y < -250 || orig.y > 250) {
                trans_vec.y = -1.0f * trans_vec.y;
            }

            glTranslatef(trans_vec.x, trans_vec.y, 0.0f);

        }
        glutPostRedisplay();
    }
}

void SimulateWorld(long old_tim, long tim){
    for(long i=old_tim; i<tim;i++){
        long te = (long)fmin(tim, i + 10);
        animate_w(i,te);
    }
}

struct ControlPoint {
    float3 m_origo_f3;
    Color m_circumfence_c, m_board_c;
    float m_radius_f;
    long m_t_l;
    float3 m_vel_f;

    ControlPoint() : m_origo_f3(), m_circumfence_c(), m_board_c(), m_radius_f(), m_t_l(), m_vel_f() {}

    ControlPoint(float3& origo, Color& circumfence, Color& board, float& radius, long& pressed_at) :
            m_origo_f3(origo), m_circumfence_c(circumfence), m_board_c(board),
            m_radius_f(radius), m_t_l(pressed_at), m_vel_f() {}

    void drawControlPoint() {
        glColor3f(m_circumfence_c.r, m_circumfence_c.g, m_circumfence_c.b);
        glBegin(GL_LINE_LOOP);
        for(int i=0; i<360; i++) {
            glVertex2f(m_origo_f3.x + m_radius_f * (float)cos(i * PI / 180.0),
                       m_origo_f3.y + m_radius_f * (float)sin(i * PI / 180.0));
        }
        glEnd();

        glColor3f(m_board_c.r, m_board_c.g, m_board_c.b);
        glBegin(GL_TRIANGLE_FAN);
        for(int i=0; i<360; i++) {
            glVertex2f(m_origo_f3.x + m_radius_f * (float)cos(i * PI / 180.0),
                       m_origo_f3.y +  m_radius_f *(float)sin(i* PI / 180.0));
        }
        glEnd();
    }
};

/* Sajat korabbi hazi feladatbol meritve */
struct CRSpline {
    float3 a0; float3 a1; float3 a2; float3 a3;

    CRSpline() : a0(), a1(), a2(), a3() {}

    void Hermite(ControlPoint& pi, ControlPoint& pii) {
        float Dt = pii.m_t_l - pi.m_t_l;
        float3 v0 = pi.m_vel_f; float3 v1 = pii.m_vel_f;
        float3 f0 = pi.m_origo_f3; float3 f1 = pii.m_origo_f3;
        this->a3 = (v1 + v0) / Dt / Dt - (f1 - f0) * 2 / Dt / Dt / Dt;
        this->a2 = (f1 - f0) * 3 / Dt / Dt - (v1 + v0*2) / Dt;
        this->a1 = v0;
        this->a0 = f0;
    }

    void drawSegment(ControlPoint& pi, ControlPoint& pii) {
        Hermite(pi, pii);
        glColor3f(1.0f, 1.0f, 1.0f);
        glBegin(GL_LINES);
        float3 crs0 = a0;
        for(int i = (int)pi.m_t_l; i <= pii.m_t_l; i++) {
            float3 crs = a3 * pow(i-pi.m_t_l, 3) + a2 * pow(i-pi.m_t_l, 2) + a1 * (i-pi.m_t_l) + a0;
            glVertex2f(crs0.x, crs0.y);
            glVertex2f(crs.x, crs.y);
            crs0 = crs;
        }
        glEnd();
    }

};

CRSpline catmull_rom = CRSpline();

struct Parabola {
    float3 seg0, seg1, seg2, vertex;
    float p;
    float angle;
    float3 itrvec;
    float3 inters_point;
    ControlPoint p0, p1, p2;
    float3 p_p[2*screenWidth];

    Parabola(): seg0(), seg1(), seg2(), vertex(), p(), itrvec() {}

    void setupParabola(ControlPoint& p0, ControlPoint& p1, ControlPoint& p2) {
        this->seg0 = p0.m_origo_f3; this->seg1 = p1.m_origo_f3; this->seg2 = p2.m_origo_f3;
        this->p0 = p0; this->p1 = p1; this->p2 = p2;
        calc();
    }

    void calc() {
        float3 line_vec = seg1 - seg0;
        float3 x_axis = float3(1.0f, 0.0f, 0.0f);
        this->angle = (float) acos(dot(line_vec, x_axis) / (line_vec.Length() * x_axis.Length()));
        if (seg0.y < seg1.y)
            this->angle = -1.0f * angle;

        float3 orig = float3(0.0f, 0.0f, 0.0f);
        float3 trvec = orig = seg0;
        this->itrvec = trvec * -1.0f;

        seg0 = translate(seg0, trvec);
        seg0 = rotateZ(seg0, angle);
        seg1 = translate(seg1, trvec);
        seg1 = rotateZ(seg1, angle);
        seg2 = translate(seg2, trvec);
        seg2 = rotateZ(seg2, angle);

        vertex = float3(seg2.x , seg2.y / 2.0f, 0.0f);

        if(seg2.y > vertex.y)
            p = seg2.y - vertex.y;
        else
            p = vertex.y - seg2.y;

        float3 parabol;

        glColor3f(1.0f, 1.0f, 0.0f);
        glBegin(GL_LINE_STRIP);
        for(int x=-screenWidth; x<screenWidth; x++) {
            if(seg2.y < vertex.y) {
                parabol = float3(x, -1.0f * (float)(pow(x - vertex.x, 2) / (4.0f * p)) + vertex.y, 0.0f);
            }
            else {
                parabol = float3(x, (float)(pow(x - vertex.x, 2) / (4.0f * p)) + vertex.y, 0.0f);
            }
            parabol = rotateZ(parabol, -1.0f * this->angle);
            parabol = translate(parabol, this->itrvec);
            glVertex2f(parabol.x, parabol.y);
            p_p[x+screenWidth] = parabol;
        }
        glEnd();

        glColor3f(0.3f, 0.1f, 0.8f);
        glBegin(GL_QUADS);
        glVertex2f(1000.0f, 0.0f);
        glVertex2f(0.0f, 0.0f);
        glVertex2f(0.0f, 1000.0f);

        glVertex2f(1000.0f, 1000.0f);
        glEnd();

        glColor3f(1.0f, 1.0f, 0.0f);
        glBegin(GL_TRIANGLE_FAN);
        for(int i=0; i<screenWidth; i++) {
            glVertex2f(p_p[screenWidth+i].x, p_p[screenWidth+i].y);
            glVertex2f(p_p[screenWidth-i].x, p_p[screenWidth-i].y);
        }
        glEnd();

        seg0 = translate(seg0, itrvec);
        seg0 = rotateZ(seg0, -angle);
        seg1 = translate(seg1, itrvec);
        seg1 = rotateZ(seg1, -angle);
        seg2 = translate(seg2, itrvec);
        seg2 = rotateZ(seg2, -angle);

    }
};

Parabola parabola;

struct Scene {
    ControlPoint m_cp[10];
    int m_cpSize;
    float3 m_parabol_points[screenWidth*2];
    Scene() : m_cpSize(0) {};
    void addControlPoint(ControlPoint& cp) {
        if (m_cpSize<9) {
            m_cp[m_cpSize] = cp;
            ++m_cpSize;
            m_cp[m_cpSize] = m_cp[0];
            for(int i = 0; i <= m_cpSize; i++)
                m_cp[i].m_vel_f = ((m_cp[i+1].m_origo_f3 - m_cp[i].m_origo_f3) /
                                   (m_cp[i+1].m_t_l - m_cp[i].m_t_l) +
                                   (m_cp[i].m_origo_f3 - m_cp[i-1].m_origo_f3) /
                                   (m_cp[i].m_t_l - m_cp[i -1].m_t_l)) * 0.5f;
        }
    }

    void drawScene() {
        if(m_cpSize > 2) {
            parabola.setupParabola(m_cp[0], m_cp[1], m_cp[2]);
        }
        for(int i=0; i<m_cpSize; i++)
            m_cp[i].drawControlPoint();
        for(int i=0; i<m_cpSize-1; i++)
            catmull_rom.drawSegment(m_cp[i], m_cp[i+1]);
        catmull_rom.drawSegment(m_cp[0], m_cp[m_cpSize-1]);
    }

};

Scene scene = Scene();
Color white = Color(1.0f, 1.0f, 1.0f);
Color red = Color(1.0f, 0.2f, 0.2f);
float circle_rad = 5.0f;

// Inicializacio, a program futasanak kezdeten, az OpenGL kontextus letrehozasa utan hivodik meg (ld. main() fv.)
void onInitialization( ) {
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluOrtho2D(0, screenWidth, 0, screenHeight);
    glViewport(0, 0, screenWidth, screenHeight);

}

// Rajzolas, ha az alkalmazas ablak ervenytelenne valik, akkor ez a fuggveny hivodik meg
void onDisplay( ) {
    glClearColor(0.1f, 0.2f, 0.3f, 1.0f);		// torlesi szin beallitasa
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // kepernyo torles

    scene.drawScene();

    glutSwapBuffers();     				// Buffercsere: rajzolas vege

}

float normalizeDeviceX(int xnd) {
    return (float)xnd / 600.0f * 1000.0f;
}

float normalizeDeviceY(int ynd) {
    return screenHeight - (float)ynd / 600.0f * 1000.0f;
}

// Billentyuzet esemenyeket lekezelo fuggveny (lenyomas)
void onKeyboard(unsigned char key, int x, int y) {
    if (key == ' '){
        scaleWorld();
        animate = true;
    }

}

// Billentyuzet esemenyeket lekezelo fuggveny (felengedes)
void onKeyboardUp(unsigned char key, int x, int y) {

}

// Eger esemenyeket lekezelo fuggveny
void onMouse(int button, int state, int x, int y) {
    float x_f = normalizeDeviceX(x);
    float y_f = normalizeDeviceY(y);
    if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {   // A GLUT_LEFT_BUTTON / GLUT_RIGHT_BUTTON illetve GLUT_DOWN / GLUT_UP
        float3 origo = float3(x_f, y_f, 0.0f);
        long pressed_at = glutGet(GLUT_ELAPSED_TIME);
        ControlPoint p = ControlPoint(origo, white, red, circle_rad, pressed_at);
        scene.addControlPoint(p);
        glutPostRedisplay();     // Ilyenkor rajzold ujra a kepet
    }
}

// Eger mozgast lekezelo fuggveny
void onMouseMotion(int x, int y)
{

}


// `Idle' esemenykezelo, jelzi, hogy az ido telik, az Idle esemenyek frekvenciajara csak a 0 a garantalt minimalis ertek
void onIdle( ) {
    d_time = i_time;
    i_time = glutGet(GLUT_ELAPSED_TIME);
    SimulateWorld(d_time, i_time);
}

// ...Idaig modosithatod
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// A C++ program belepesi pontja, a main fuggvenyt mar nem szabad bantani
int main(int argc, char **argv) {
    glutInit(&argc, argv); 				// GLUT inicializalasa
    glutInitWindowSize(600, 600);			// Alkalmazas ablak kezdeti merete 600x600 pixel 
    glutInitWindowPosition(100, 100);			// Az elozo alkalmazas ablakhoz kepest hol tunik fel
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);	// 8 bites R,G,B,A + dupla buffer + melyseg buffer

    glutCreateWindow("Grafika hazi feladat");		// Alkalmazas ablak megszuletik es megjelenik a kepernyon

    glMatrixMode(GL_MODELVIEW);				// A MODELVIEW transzformaciot egysegmatrixra inicializaljuk
    glLoadIdentity();
    glMatrixMode(GL_PROJECTION);			// A PROJECTION transzformaciot egysegmatrixra inicializaljuk
    glLoadIdentity();

    onInitialization();					// Az altalad irt inicializalast lefuttatjuk

    glutDisplayFunc(onDisplay);				// Esemenykezelok regisztralasa
    glutMouseFunc(onMouse);
    glutIdleFunc(onIdle);
    glutKeyboardFunc(onKeyboard);
    glutKeyboardUpFunc(onKeyboardUp);
    glutMotionFunc(onMouseMotion);

    glutMainLoop();					// Esemenykezelo hurok

    return 0;
}

